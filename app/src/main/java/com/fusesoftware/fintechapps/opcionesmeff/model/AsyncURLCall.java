package com.fusesoftware.fintechapps.opcionesmeff.model;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by mllorente on 18/02/2016.
 * Referencia: http://stackoverflow.com/questions/12575068/how-to-get-the-result-of-onpostexecute-to-main-activity-because-asynctask-is-a
 *
 * o bien
 *
 * http://stackoverflow.com/questions/9963691/android-asynctask-sending-callbacks-to-ui
 *
 *
 * //TODO Actualizar para que use "Option", "Call" y "Put"
 *
 *
 */
public class AsyncURLCall extends AsyncTask<String,Void,ArrayList<OptionRow>> {
    private final String TAG = this.getClass().getName();
    // you may separate this or combined to caller class.
    public interface AsyncResponse {
        void urlCallFinish(ArrayList<OptionRow> output);
    }

    public AsyncResponse delegate = null;

    public AsyncURLCall(AsyncResponse delegate){
        this.delegate = delegate;
    }
    String content, address;
    @Override
    protected ArrayList<OptionRow> doInBackground(String... urls) {
        StringBuffer sb = new StringBuffer();
        if(urls.length>1){
            //TODO raise and exception or develop loops
        }
        address = urls[0];
        try {
            sb.append(getData(address));
            } catch (Exception e) {
                e.printStackTrace();
            }
            printFormattedWithJsoup(sb.toString(),address);


        content = sb.toString();
        return loadRowsIntoAList(sb.toString(), address);
    }

    @Override
    protected void onPostExecute(ArrayList<OptionRow> optionRows) {
        delegate.urlCallFinish(optionRows);
    }

    protected void printFormattedWithJsoup(String html, String url) {
        Document doc = Jsoup.parse(html, url);
        System.out.println("JSOUP-->DOCUMENT TITLE " + doc.title() + " of url " + url);
        org.jsoup.nodes.Element tabla = doc.getElementById("ctl00_Contenido_tblOpAmericanas");
        if(tabla == null){
            return;
        }
        System.out.println("JSOUP-->Table ctl00_Contenido_tblOpAmericanas contains " + tabla.getAllElements().size() + " elements.");

        //Elements tableElements = tabla.getAllElements();
        //Explicación: he buscado con tabla.getallElements() el elemento y luego he obtenido su cssSelector() para saber lo que me
        // daba la fila exacta que buscamos.
        Elements rows = tabla.select("tr.oc");
        int k = 1;
        for (Element element : rows) {
            System.out.println("JSOUP-->" + "[" + k + "]"
                    + ": " + element.text());
            k++;
        }
    }

    protected ArrayList<OptionRow> loadRowsIntoAList(String html, String url){
        ArrayList<OptionRow> returnList = new ArrayList<>();
        Document doc = Jsoup.parse(html, url);
        if(doc!=null) {
            org.jsoup.nodes.Element tabla = doc.getElementById("ctl00_Contenido_tblOpAmericanas");
            Elements rows = tabla.select("tr.oc");
            Log.d(TAG, "Detectados " + rows.size() + " elementos.");
            for (Element element : rows) {

                //Log.d(TAG, "URL is->" + url);
                //Log.d(TAG, "adding new OpionRow->" + element.text());
                returnList.add(new OptionRow(element.text(), url.substring(url.length() - 3)));
            }
        }
        return returnList;
    }

    private String getData(String address) throws Exception {
        //TextView box = (TextView)findViewById(R.id.text);
        //setTitle(address);
        StringBuilder sb = new StringBuilder();
        URL page = new URL(address);
        StringBuffer text = new StringBuffer();
        HttpURLConnection conn = (HttpURLConnection) page.openConnection();
        conn.connect();
        InputStreamReader in = new InputStreamReader((InputStream) conn.getContent());
        BufferedReader buff = new BufferedReader(in);
        Log.i(TAG, "Getting data...");
        //box.setText("Getting data ...");
        String line="Hi";
        do {
            line = buff.readLine();
            sb.append(line);
            if(line!=null) {
                Log.i(TAG, line);
            }
        } while (line != null);
        //box.setText(text.toString());
        return sb.toString();
    }

}
