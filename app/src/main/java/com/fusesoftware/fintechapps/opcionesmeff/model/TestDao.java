package com.fusesoftware.fintechapps.opcionesmeff.model;

import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Miguel A. Llorente on 23/06/2016.
 */
public class TestDao {

    public void test() {

        String ticker = "TEST";
        DateTime now = DateTime.now();

        Log.d("HOLA", "QUE ALGUIEN ME HAGA CASO");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 30);
        Date updateDate1630 = new Date(calendar.getTimeInMillis());


        Option option1 = new Option(OptionType.CALL, ticker, 1, 1, 1, 1, 1, 1, 1.1f, new Date(now.plusDays(1).getMillis()), updateDate1630);
        Option option2 = new Option(OptionType.CALL, ticker, 1, 1, 1, 1, 1, 1, 1.2f, new Date(now.plusDays(2).getMillis()), updateDate1630);
        Option option3 = new Option(OptionType.CALL, ticker, 1, 1, 1, 1, 1, 1, 1.4f, new Date(now.plusDays(4).getMillis()), updateDate1630);
        Option option4 = new Option(OptionType.CALL, ticker, 1, 1, 1, 1, 1, 1, 1.6f, new Date(now.plusDays(6).getMillis()), updateDate1630);
        Option option5 = new Option(OptionType.CALL, ticker + "ERROR", 1, 1, 1, 1, 1, 1, 1.4f, new Date(now.plusDays(4).getMillis()), updateDate1630);

        option1.save();
        option2.save();
        option3.save();
        option4.save();
        option5.save();

        Log.d("TESTING EXPIRATION", "" + "Expected 1 and get..." + OptionDAO.getCallFromExpiration(ticker, now, now.plusDays(1)).size());
        Log.d("TESTING EXPIRATION", "" + "Expected 2 and get..." + OptionDAO.getCallFromExpiration(ticker, now, now.plusDays(2)).size());
        Log.d("TESTING EXPIRATION", "" + "Expected 3 and get..." + OptionDAO.getCallFromExpiration(ticker, now, now.plusDays(4)).size());
        Log.d("TESTING EXPIRATION", "" + "Expected 4 and get..." + OptionDAO.getCallFromExpiration(ticker, now, now.plusDays(6)).size());

        Log.d("TESTING STRIKE", "" + "Expected 1 and get..." + OptionDAO.getCallFromStrike(ticker, 0.9f, 1.2f).size());
        Log.d("TESTING STRIKE", "" + "Expected 2 and get..." + OptionDAO.getCallFromStrike(ticker, 0.9f, 1.3f).size());
        Log.d("TESTING STRIKE", "" + "Expected 3 and get..." + OptionDAO.getCallFromStrike(ticker, 0.9f, 1.5f).size());
        Log.d("TESTING STRIKE", "" + "Expected 4 and get..." + OptionDAO.getCallFromStrike(ticker, 0.9f, 1.7f).size());

        Log.d("TESTING LAST", "Expected last with strike 1.6 ticker and get..." + OptionDAO.getLastStored(ticker).toString());

        /**
        ArrayList<Option> list = (ArrayList<Option>) OptionDAO.getLastStored(ticker);
        for (Option opt : list
             ) {
            Log.d("DEBUGGING LAST",opt.toString());
        }
        **/

        Option.delete(option1);
        Option.delete(option2);
        Option.delete(option3);
        Option.delete(option4);
        Option.delete(option5);
    }
}
