package com.fusesoftware.fintechapps.opcionesmeff;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusesoftware.fintechapps.opcionesmeff.model.OptionRow;

import java.util.ArrayList;

/**
 * Created by mllorente on 15/02/2016.
 *
 * Referencia: http://techlovejump.com/android-multicolumn-listview/
 *
 */
public class ListViewAdapter extends ArrayAdapter<OptionRow> {

    public ArrayList<OptionRow> rowlist;

    Activity activity;
    TextView txtUpdateHour;
    TextView txtVolBuyC;
    TextView txtPriceBuyC;
    TextView txtPriceSellC;
    TextView txtVolSellC;
    TextView txtStrike;
    TextView txtDeadline;
    TextView txtVolBuyP;
    TextView txtPriceBuyP;
    TextView txtPriceSellP;
    TextView txtVolSellP;



    public ListViewAdapter(Context context, ArrayList<OptionRow> optionRows){
        super(context, 0, optionRows);
        rowlist = optionRows;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rowlist.size();
    }

    @Override
    public OptionRow getItem(int position) {
        // TODO Auto-generated method stub
        return rowlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater=activity.getLayoutInflater();
        if(convertView == null){
            convertView=inflater.inflate(R.layout.column_row, null);
            txtUpdateHour = (TextView) convertView.findViewById(R.id.updateHour);

            txtVolBuyC      =(TextView) convertView.findViewById(R.id.volBuyCall);
            txtPriceBuyC    =(TextView) convertView.findViewById(R.id.priceBuyCall);
            txtPriceSellC   =(TextView) convertView.findViewById(R.id.priceSellCall);
            txtVolSellC     =(TextView) convertView.findViewById(R.id.volSellCall);

            txtStrike       =(TextView) convertView.findViewById(R.id.strike);
            txtDeadline     =(TextView) convertView.findViewById(R.id.deadline);

            txtVolBuyP      =(TextView) convertView.findViewById(R.id.volBuyPut);
            txtPriceBuyP    =(TextView) convertView.findViewById(R.id.priceBuyPut);
            txtPriceSellP   =(TextView) convertView.findViewById(R.id.priceSellPut);
            txtVolSellP     =(TextView) convertView.findViewById(R.id.volSellPut);

        }
        OptionRow row = rowlist.get(position);
        System.out.println(row.toString());
        txtUpdateHour.setText(row.getUpdateTime());
        txtVolBuyC.setText(row.getBuyVolumeCall());
        txtPriceBuyC.setText(row.getBuyPriceCall());
        txtPriceSellC.setText(row.getSellPriceCall());
        txtVolSellC.setText(row.getSellVolumeCall());
        txtStrike.setText(row.getStrike());
        txtDeadline.setText(row.getDeadline());
        txtVolBuyP.setText(row.getBuyVolumePut());
        txtPriceBuyP.setText(row.getBuyPricePut());
        txtPriceSellP.setText(row.getSellPricePut());
        txtVolSellP.setText(row.getSellVolumePut());
        return convertView;
    }
}