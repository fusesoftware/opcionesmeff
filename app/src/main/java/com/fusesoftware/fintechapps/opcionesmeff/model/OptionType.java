package com.fusesoftware.fintechapps.opcionesmeff.model;

/**
 * Created by mllorente on 15/02/2016.
 */
public enum OptionType {

    PUT("PUT"),CALL("CALL");

    private String name;


    OptionType(String name){
    this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String   toString() {
        return name;
    }
}
