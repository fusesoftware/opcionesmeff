package com.fusesoftware.fintechapps.opcionesmeff.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.CommonConfig;
import com.fusesoftware.fintechapps.opcionesmeff.model.MarketCloseManager;

/**
 * Created by Miguel A. Llorente on 04/07/2016.
 */
public class DeviceBootReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            new MarketCloseManager(context).scheduleLastUpdateAt(CommonConfig.lastUpdateHour, CommonConfig.lastUpdateMinutes);
        }
    }

}
