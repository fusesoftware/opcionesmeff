package com.fusesoftware.fintechapps.opcionesmeff.model;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.ValuesES;

/**
 * Created by mllorente on 11/02/2016.
 */
public class StockID {

    private String name;
    private String ticker;
    private String isin;

    public StockID(String name, String ticker, String isin) {
        this.name = name;
        this.ticker = ticker;
        this.isin = isin;
    }

    public String getTicker(String name){
        return ValuesES.stockIdList.get(getStockIndexInList(name)).getTicker();
    }


    private int getStockIndexInList(String queryValue){

        for(int i = 0; i<ValuesES.stockIdList.size();i++){
            StockID stockID = ValuesES.stockIdList.get(i);
            if(stockID.getName().equals(queryValue)||stockID.getTicker().equals(queryValue)||stockID.getIsin().equals(queryValue)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }
}
