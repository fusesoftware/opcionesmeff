package com.fusesoftware.fintechapps.opcionesmeff.model;

import com.orm.query.Condition;
import com.orm.query.Select;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Miguel A. Llorente on 23/06/2016.
 *
 * Estoy haciendo un panel de configuración para las llamadas a la base de datos. Te paso una captura de pantalla.

 Básicamente se podrá elegir lo siguiente:

 - Solo puts, solo calls o ambos
 - Mostrar volumen del día
 - Filtrar por rango de vencimientos
 - Filtrar por rango de strike

 De momento con eso va que chuta.

 Sabla.


 *
 */
public class OptionDAO {

    private static long ONE_HOUR_IN_MILLIS = 60*60*1000;

    public OptionDAO(){}

    public static List<Option> getCallFromExpiration(String ticker, DateTime initialTime, DateTime finalTime) {
        //works
        return Option.find(Option.class,"type = ? and ticker = ? and expiration >= ? and expiration <= ?",OptionType.CALL.toString(),ticker,initialTime.toString(),finalTime.toString());
        //works
        //return Option.listAll(Option.class);
    }

    public static List<Option> getCallFromStrike(String ticker, float minStrike, float maxStrike){
        return Option.find(Option.class,"type = ? and ticker = ? and strike >= ? and strike <= ?",OptionType.CALL.toString(),ticker,""+minStrike,""+maxStrike);
    }


    public static List<Option> getPutFromExpiration(String ticker, DateTime initialTime, DateTime finalTime) {
        //works
        return Option.find(Option.class,"type = ? and ticker = ? and expiration >= ? and expiration <= ?",OptionType.PUT.toString(), ticker,initialTime.toString(),finalTime.toString());
        //works
        //return Option.listAll(Option.class);
    }

    public static List<Option> getPutFromStrike(String ticker, float minStrike, float maxStrike){
        return Option.find(Option.class,"type = ? and ticker = ? and strike >= ? and strike <= ?",OptionType.PUT.toString(),ticker,""+minStrike,""+maxStrike);
    }

    public static Option getLastStored(String ticker){
        //Select.from(Option.class).where(Condition.prop("ticker"))
            return Option.find(Option.class,"ticker = ?",new String[]{ticker},null,"strike DESC","1").get(0);
    }

    /**
     * TODO Check the algorithm
     * Returns true if the data in database is updated.
     * Date is considered updated if is younger than an hour and updateTime coincides.
     * @param ticker Stock ticker
     * @return true if updated, false otherwise
     */
    public static boolean isDBUpdated(String ticker){
        Option last = getLastStored(ticker);
        Calendar nowTime = Calendar.getInstance();
        long nowTimeInMillis = nowTime.getTimeInMillis();
        int nowTimeMinutes = nowTime.get(Calendar.MINUTE);
        Calendar lastTime = Calendar.getInstance();
        lastTime.setTimeInMillis(last.getTimestamp());

        Calendar lastUpdateTime = Calendar.getInstance();
        lastUpdateTime.setTimeInMillis(last.getUpdateDate().getTime());

        int lastUpdateTimeMinutes = lastUpdateTime.get(Calendar.MINUTE);

        long timeDiff = nowTimeInMillis - last.getTimestamp();

        if(timeDiff<ONE_HOUR_IN_MILLIS && lastUpdateTimeMinutes%15 == nowTimeMinutes%15){
            return true;
        }else{
            return false;
        }





    }

}
