package com.fusesoftware.fintechapps.opcionesmeff.model;

import com.orm.SugarRecord;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Date;

/**
 * Created by julian on 13/02/16.
 */
public class Option extends SugarRecord {

    private OptionType type;

    private String ticker;
    private float bidPrice;
    private int bidVolume;
    private float askPrice;
    private float askVolume;
    //Volumen - Filtrar
    private int dayVolume;
    private float lastPrice;
    //Strike - Filtrar
    private float strike;
    //Vencimiento - Filtrar
    private Date expiration;
    private int daysToExpire;



    private Date updateDate;
    private long timestamp;

    public Option(){
    }

    public Option(OptionType type, String ticker, float bidPrice, int bidVolume, float askPrice, float askVolume, int dayVolume, float lastPrice, float strike, Date expiration, Date updateDate) {
        this.type           = type;
        this.ticker         = ticker;
        this.bidPrice       = bidPrice;
        this.bidVolume      = bidVolume;
        this.askPrice       = askPrice;
        this.askVolume      = askVolume;
        this.dayVolume      = dayVolume;
        this.lastPrice      = lastPrice;
        this.strike         = strike;
        this.expiration     = expiration;
        DateTime expirationDateTime = new DateTime(expiration.getTime());
        this.daysToExpire   = Days.daysBetween(DateTime.now(),expirationDateTime).getDays();
        this.timestamp      = DateTime.now().getMillis();
        this.updateDate     = updateDate;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Option type: ");
        sb.append(type        );
        sb.append("\nticker: ");
        sb.append(ticker      );
        sb.append("\nbidPrice: ");
        sb.append(bidPrice    );
        sb.append("\nbidVolume: ");
        sb.append(bidVolume   );
        sb.append("\naskPrice: ");
        sb.append(askPrice    );
        sb.append("\naskVolume: ");
        sb.append(askVolume   );
        sb.append("\ndayVolume: ");
        sb.append(dayVolume   );
        sb.append("\nlastPrice: ");
        sb.append(lastPrice   );
        sb.append("\nstrike: ");
        sb.append(strike      );
        sb.append("\nexpiration: ");
        sb.append(expiration  );
        sb.append("\ndaysToExpire: ");
        sb.append(daysToExpire);
        sb.append("\ntimestamp: ");
        sb.append(timestamp   );
        sb.append("\nupdateDate: ");
        sb.append(updateDate  );
        sb.append("\n--END--");
        return sb.toString();
    }

    public OptionType getType() {
        return type;
    }

    public void setType(OptionType type) {
        this.type = type;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String stockId) {
        this.ticker = ticker;
    }

    public int getBidVolume() {
        return bidVolume;
    }

    public void setBidVolume(int bidVolume) {
        this.bidVolume = bidVolume;
    }

    public float getAskVolume() {
        return askVolume;
    }

    public void setAskVolume(float askVolume) {
        this.askVolume = askVolume;
    }

    public double getStrike() {
        return strike;
    }

    public void setStrike(float strike) {
        this.strike = strike;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public int getDaysToExpire() {
        return daysToExpire;
    }

    public void setDaysToExpire(int daysToExpire) {
        this.daysToExpire = daysToExpire;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(float bidPrice) {
        this.bidPrice = bidPrice;
    }

    public double getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(float askPrice) {
        this.askPrice = askPrice;
    }

    public int getDayVolume() {
        return dayVolume;
    }

    public void setDayVolume(int dayVolume) {
        this.dayVolume = dayVolume;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(float lastPrice) {
        this.lastPrice = lastPrice;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
