package com.fusesoftware.fintechapps.opcionesmeff;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.CommonConfig;
import com.fusesoftware.fintechapps.opcionesmeff.model.MarketCloseManager;
import com.fusesoftware.fintechapps.opcionesmeff.receiver.AlarmReceiver;

import java.util.Calendar;

/**
 * Created by Miguel A. Llorente on 05/07/2016.
 */
public class TestAlarm {

    public static void test(Context context){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        PendingIntent pendingIntent;
        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        SharedPreferences preferences = context.getSharedPreferences(CommonConfig.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CommonConfig.PREF_MARKET_CLOSE_ALARM,pendingIntent.getCreatorUid()).commit();
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        /* Set the alarm to start at a given hour*/
        Calendar customTime = Calendar.getInstance();
        customTime.setTimeInMillis(System.currentTimeMillis());

        manager.set(AlarmManager.RTC_WAKEUP,customTime.getTimeInMillis()+1000,pendingIntent);

        try {
            Thread.sleep(180000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        PendingIntent pendingIntentCancel;
        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntentCancel = new Intent(context, AlarmReceiver.class);
        pendingIntentCancel = PendingIntent.getBroadcast(context, 0, alarmIntentCancel, 0);

        manager.cancel(pendingIntentCancel);

    }

}
