package com.fusesoftware.fintechapps.opcionesmeff.data.common;

import android.util.Log;

import com.fusesoftware.fintechapps.opcionesmeff.model.StockID;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mllorente on 11/02/2016.
 * Updated by mllorente on 11/02/2016.
 * TODO: Update IBEX35 Values automatically
 */
public class ValuesES {

    public static ArrayList<StockID> stockIdList = new ArrayList<>();

    private static String[] IBEX35_NAMES = {
           "Abertis Infraestructuras",
           "Acciona",
           "Acerinox",
           "Actividades de Construcción y Servicios",
           "AENA",
           "Amadeus",
           "Arcelor Mittal",
           "Banco Popular Español",
           "Banco de Sabadell",
           "Banco Santander",
           "Bankia",
           "Bankinter",
           "Banco Bilbao Vizcaya Argentaria",
           "CaixaBank",
           "Distribuidora Internacional de Alimentación",
           "Enagás",
           "Endesa",
           "Fomento de Construcciones y Contratas",
           "Ferrovial",
           "Gamesa Corporación Tecnológica",
           "Gas Natural SDG",
           "Grifols",
           "International Airlines Group",
           "Iberdrola",
           "Inditex",
           "Indra Sistemas",
           "MAPFRE",
           "Mediaset España Comunicación",
           "MERLIN Properties",
           "Obrascón Huarte Lain",
           "Red Eléctrica Corporación",
           "Repsol",
           "Sacyr",
           "Técnicas Reunidas",
           "Telefónica"
    };

    private static String[] IBEX35_TICKERS = {
           "ABE",
           "ANA",
           "ACX",
           "ACS",
           "AENA",
           "AMS",
           "MTS",
           "POP",
           "SAB",
           "SAN",
           "BKIA",
           "BKT",
           "BBVA",
           "CABK",
           "DIA",
           "ENG",
           "ELE",
           "FCC",
           "FER",
           "GAM",
           "GAS",
           "GRF",
           "IAG",
           "IBE",
           "ITX",
           "IDR",
           "MAP",
           "TL5",
           "MRL",
           "OHL",
           "REE",
           "REP",
           "SCYR",
           "TRE",
           "TEF"
    };

    private static LinkedList<String> tickerList = new LinkedList<>();

    public static List getTickerList(){
        return tickerList;
    }

        private static String[] IBEX35_ISINS = {
               "ES0111845014",
               "ES0125220311",
               "ES0132105018",
               "ES0167050915",
               "ES0105046009",
               "ES0109067019",
               "LU0323134006",
               "ES0113790226",
               "ES0113860A34",
               "ES0113900J37",
               "ES0113307021",
               "ES0113679I37",
               "ES0113211835",
               "ES0140609019",
               "ES0126775032",
               "ES0130960018",
               "ES0130670112",
               "ES0122060314",
               "ES0118900010",
               "ES0143416115",
               "ES0116870314",
               "ES0171996012",
               "ES0177542018",
               "ES0144580Y14",
               "ES0148396015",
               "ES0118594417",
               "ES0124244E34",
               "ES0152503035",
               "ES0105025003",
               "ES0142090317",
               "ES0173093115",
               "ES0173516115",
               "ES0182870214",
               "ES0178165017",
               "ES0178430E18"
        };

    public static void init(){
        Log.i("ValuesEs", "INIT CALLED");
        if(IBEX35_NAMES.length == IBEX35_TICKERS.length && IBEX35_ISINS.length == IBEX35_TICKERS.length && IBEX35_ISINS
                .length==IBEX35_TICKERS.length) {
            for (int i = 0; i < IBEX35_NAMES.length; i++) {
                Log.i("ValuesEs", "Adding:"+IBEX35_NAMES[i]+" "+IBEX35_TICKERS[i]+" "+IBEX35_ISINS[i]);
                stockIdList.add(new StockID(IBEX35_NAMES[i], IBEX35_TICKERS[i], IBEX35_ISINS[i]));
            }
            for (int i = 0; i < IBEX35_TICKERS.length; i++) {
                getTickerList().add(IBEX35_TICKERS[i]);
                }
        }else{
            Log.e("ValuesEs", "Something went wrong");
            //TODO raise an exception
        }
    }

}
