package com.fusesoftware.fintechapps.opcionesmeff.data.common;

/**
 * Created by Miguel A. Llorente on 04/07/2016.
 */
public class CommonConfig {

    public static final int lastUpdateHour = 17;
    public static final int lastUpdateMinutes = 20;
    public static final int updatePeriod = 1000 * 60 * 2;
    public static final int market_start_hour = 9;
    public static final int market_start_minutes = 0;
    public static final int market_close_hour = 17;
    public static final int market_close_minutes = 30;
    public static final String SHARED_PREFERENCES = "com.fusesoftware.fintechapps.opcionesmeff.shared_preferences";
    public static final String PREF_MARKET_CLOSE_ALARM = "com.fusesoftware.fintechapps.opcionesmeff.pref_market_close_alarm";
}
