package com.fusesoftware.fintechapps.opcionesmeff;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.CommonConfig;
import com.fusesoftware.fintechapps.opcionesmeff.data.common.ValuesES;
import com.fusesoftware.fintechapps.opcionesmeff.model.AsyncURLCall;
import com.fusesoftware.fintechapps.opcionesmeff.model.Option;
import com.fusesoftware.fintechapps.opcionesmeff.model.OptionRow;
import com.fusesoftware.fintechapps.opcionesmeff.model.StockID;
import com.fusesoftware.fintechapps.opcionesmeff.model.TestDao;
import com.fusesoftware.fintechapps.opcionesmeff.receiver.AlarmReceiver;
import com.fusesoftware.fintechapps.opcionesmeff.services.UpdateService;
import com.orm.SugarContext;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by julian on 13/03/16.
 *
 *
 * //TODO Actualizar para que utilice los DAOs
 *
 */
public class OptionDataFragment extends Fragment{

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;
    boolean showDailyVolume;
    short showPutsCalls;
    boolean showMidPoint_BidAsk;
    boolean showTableStacked_Straddle;
    int nearExpirationDateIndex, farExpirationDateIndex;
    int lowStrikePriceIndex, highStrikePriceIndex;

    private static final short SHOW_PUTS_ONLY = 0, SHOW_CALLS_ONLY=1, SHOW_PUTS_CALLS=2;
    private static final boolean MIDPOINT = false, BIDASK = true, STACKED = false, STRADDLE = true;

    private ArrayList<Float> strikes;
    private ArrayList<Date> expirationDates;

    private TableLayout optionTable;
    private TableLayout optionTableHeader;
    private ArrayList<OptionRow> optionRows;

    AsyncURLCall getStockDataFromUrl;

    //Test JobSchedule
    private static int kJobId = 0;
    private UpdateService myService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_option_data_fragment,container,false);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        //Initialize the list of Spanish values and List View
        ValuesES.init();
        final Spinner spinner = (Spinner) view.findViewById(R.id.listaTickers);
        ArrayList<String> companyNames = new ArrayList<>();
        for (StockID stockid :ValuesES.stockIdList) {
            companyNames.add(stockid.getName());
        }

        // Create an ArrayAdapter using the StockID array and a default spinner layout
        ArrayAdapter<StockID> adapter = new ArrayAdapter<>(view.getContext(),android.R.layout.simple_spinner_item,ValuesES.stockIdList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(context,"Company selected:\n" + ValuesES.stockIdList.get(position).getName() +
                        "\nTicker: " + ValuesES.stockIdList.get(position).getTicker() ,Toast.LENGTH_SHORT).show();

                new AsyncURLCall(new AsyncURLCall.AsyncResponse() {
                    @Override
                    public void urlCallFinish(ArrayList<OptionRow> output) {
                        Set<Float> strikeSet = new LinkedHashSet<Float>();
                        Set<Date> dateSet = new LinkedHashSet<Date>();
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                        for(OptionRow optionRow: output) {
                            strikeSet.add(Float.parseFloat(optionRow.getStrike()));
                            try {
                                dateSet.add(format.parse(optionRow.getDeadline()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        strikes.clear();
                        expirationDates.clear();

                        for(int i=0; i< strikeSet.size();i++) {
                            strikes.add((Float) strikeSet.toArray()[i]);
                            Log.d("MEFF","Strike Set: " + strikeSet.toArray()[i]);
                        }

                        for(int i=0; i< dateSet.size();i++) {
                            expirationDates.add((Date) dateSet.toArray()[i]);
                            Log.d("MEFF","Expiration Set: " + dateSet.toArray()[i]);
                        }

                        Collections.sort(strikes);
                        Collections.sort(expirationDates);
                        lowStrikePriceIndex=0;
                        highStrikePriceIndex=strikes.size()-1;
                        nearExpirationDateIndex=0;
                        farExpirationDateIndex=expirationDates.size()-1;
                        optionRows = output;

                        refreshTable();
                        //Grabar en base de datos (FIXME)
                        for (OptionRow opt : output
                             ) {
                            opt.saveRow();
                        }
                    }
                }).execute("http://www.meff.es/aspx/Financiero/Ficha.aspx?ticker=" +
                        ValuesES.stockIdList.get(spinner.getSelectedItemPosition()).getTicker());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        optionTable = (TableLayout) view.findViewById(R.id.option_table);
        optionTableHeader= (TableLayout) view.findViewById(R.id.table_header);
        //optionTable.setColumnCollapsed(2,true);

        expirationDates = new ArrayList<>();
        strikes =  new ArrayList<>();
        /*// DEBUG---------------------------------------------------------
        final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
        Date date = new Date();
        try {
            date = format.parse("15-07-16");
            expirationDates.add(date);
            date = format.parse("19-08-16");
            expirationDates.add(date);
            date= format.parse("16-09-16");
            expirationDates.add(date);
            strikes.add(3.00f);
            strikes.add(3.10f);
            strikes.add(3.20f);
            strikes.add(3.30f);
            strikes.add(3.40f);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        nearExpirationDateIndex = 1;
        farExpirationDateIndex = 2;
        lowStrikePriceIndex = 2;
        highStrikePriceIndex = 3;
        //----------------------*/

        showDailyVolume = false;
        showPutsCalls = SHOW_PUTS_CALLS;
        showMidPoint_BidAsk = BIDASK;
        showTableStacked_Straddle = STRADDLE;

        Button button = (Button) view.findViewById(R.id.settingsButton);
        button.setOnClickListener(new View.OnClickListener() {
            short showPutsCallsTemp;
            boolean showMidPoint_BidAskTemp, showTableStacked_StraddleTemp, showDailyVolumeTemp;
            int nearExpirationDateIndexTemp, farExpirationDateIndexTemp, lowStrikePriceIndexTemp, highStrikePriceIndexTemp;
            @Override
            public void onClick(View v) {
                showDailyVolumeTemp = showDailyVolume;
                showPutsCallsTemp = showPutsCalls;
                showMidPoint_BidAskTemp = showMidPoint_BidAsk;
                showTableStacked_StraddleTemp = showTableStacked_Straddle;
                nearExpirationDateIndexTemp = nearExpirationDateIndex;
                farExpirationDateIndexTemp = farExpirationDateIndex;
                lowStrikePriceIndexTemp = lowStrikePriceIndex;
                highStrikePriceIndexTemp = highStrikePriceIndex;

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.tab_option_data_config_dialog);
                //dialog.setTitle("Table settings");

                Button dialogButtonCancel = (Button) dialog.findViewById(R.id.settingsCancelButton);
                dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                Button dialogButtonOk = (Button) dialog.findViewById(R.id.settingsOkButton);
                dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Apply new settings
                        showDailyVolume = showDailyVolumeTemp;
                        showMidPoint_BidAsk = showMidPoint_BidAskTemp;
                        showPutsCalls = showPutsCallsTemp;
                        showTableStacked_Straddle = showTableStacked_StraddleTemp;
                        nearExpirationDateIndex = nearExpirationDateIndexTemp;
                        farExpirationDateIndex = farExpirationDateIndexTemp;
                        lowStrikePriceIndex = lowStrikePriceIndexTemp;
                        highStrikePriceIndex = highStrikePriceIndexTemp;
                        Log.d("OpcionesMEFF","Config is:\n Daily Volume: " + showDailyVolume +"\n Put-Call " + showPutsCalls + "\n BidAsk: " + showMidPoint_BidAsk +
                        "\n Straddle: " + showTableStacked_Straddle + "\n Expiration dates: " + nearExpirationDateIndex + "--" + farExpirationDateIndex + "\n Strikes: " +
                        lowStrikePriceIndex + "--" + highStrikePriceIndex);
                        refreshTable();
                        dialog.dismiss();
                    }
                });

                CheckBox dialogVolumeCheckbox = (CheckBox) dialog.findViewById(R.id.volumeCheckBox);
                dialogVolumeCheckbox.setChecked(showDailyVolumeTemp);
                dialogVolumeCheckbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDailyVolumeTemp = ((CheckBox) v).isChecked();
                    }
                });

                View.OnClickListener radioButtonsListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch(v.getId()){
                            case R.id.radioButtonBidAsk:
                                showMidPoint_BidAskTemp = BIDASK;
                                break;
                            case R.id.radioButtonMidPoint:
                                showMidPoint_BidAskTemp = MIDPOINT;
                                break;
                            case R.id.radioButtonCalls:
                                showPutsCallsTemp = SHOW_CALLS_ONLY;
                                break;
                            case R.id.radioButtonPuts:
                                showPutsCallsTemp = SHOW_PUTS_ONLY;
                                break;
                            case R.id.radioButtonPutsCalls:
                                showPutsCallsTemp = SHOW_PUTS_CALLS;
                                break;
                            case R.id.radioButtonStacked:
                                showTableStacked_StraddleTemp = STRADDLE;
                                break;
                            case R.id.radioButtonStraddle:
                                showTableStacked_StraddleTemp = STACKED;
                                break;
                        }
                    }
                };

                RadioButton radioBidAsk = (RadioButton) dialog.findViewById(R.id.radioButtonBidAsk);
                radioBidAsk.setOnClickListener(radioButtonsListener);
                RadioButton radioMidPoint = (RadioButton) dialog.findViewById(R.id.radioButtonMidPoint);
                radioMidPoint.setOnClickListener(radioButtonsListener);
                RadioButton radioCalls = (RadioButton) dialog.findViewById(R.id.radioButtonCalls);
                radioCalls.setOnClickListener(radioButtonsListener);
                RadioButton radioPuts = (RadioButton) dialog.findViewById(R.id.radioButtonPuts);
                radioPuts.setOnClickListener(radioButtonsListener);
                RadioButton radioPutsCalls = (RadioButton) dialog.findViewById(R.id.radioButtonPutsCalls);
                radioPutsCalls.setOnClickListener(radioButtonsListener);
                RadioButton radioStraddle = (RadioButton) dialog.findViewById(R.id.radioButtonStacked);
                radioStraddle.setOnClickListener(radioButtonsListener);
                RadioButton radioStacked = (RadioButton) dialog.findViewById(R.id.radioButtonStraddle);
                radioStacked.setOnClickListener(radioButtonsListener);

                if(showMidPoint_BidAskTemp == BIDASK){
                    radioBidAsk.setChecked(true);
                } else {
                    radioMidPoint.setChecked(true);
                }

                if(showTableStacked_StraddleTemp == STACKED){
                    radioStacked.setChecked(true);
                } else {
                    radioStraddle.setChecked(true);
                }

                switch(showPutsCallsTemp){
                    case SHOW_CALLS_ONLY:
                        radioCalls.setChecked(true);
                        break;
                    case SHOW_PUTS_CALLS:
                        radioPutsCalls.setChecked(true);
                        break;
                    case SHOW_PUTS_ONLY:
                        radioPuts.setChecked(true);
                        break;
                    default:
                        break;
                }


                RangeSeekBar<Integer> expirationSeekBar = (RangeSeekBar<Integer>) dialog.findViewById(R.id.expirationSeekBar);
                expirationSeekBar.setRangeValues(0,expirationDates.size()-1);
                expirationSeekBar.setSelectedMinValue(nearExpirationDateIndexTemp);
                expirationSeekBar.setSelectedMaxValue(farExpirationDateIndexTemp);
                expirationSeekBar.setNotifyWhileDragging(true);
                expirationSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    Toast toast;
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                        //Log.d("OpcionesMEFF","Valores obtenidos: Min = " + minValue + " Max = " + maxValue);
                        //expirationSeekBar.setSelectedMinValue(1.00f);
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
                        if(toast == null) {
                            toast = Toast.makeText(context, "EXPIRATION\n" + expirationDates.get(minValue) + " --- " + expirationDates.get(maxValue) + "\n", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL,0, -200);
                        }
                        nearExpirationDateIndexTemp = minValue;
                        farExpirationDateIndexTemp = maxValue;
                        toast.setText("EXPIRATION\n" + format.format(expirationDates.get(minValue)) + " <-> " + format.format(expirationDates.get(maxValue)));
                        toast.show();
                    }

                });


                RangeSeekBar<Integer> strikeSeekBar = (RangeSeekBar<Integer>) dialog.findViewById(R.id.strikeSeekBar);
                strikeSeekBar.setRangeValues(0,strikes.size()-1);
                strikeSeekBar.setSelectedMinValue(lowStrikePriceIndexTemp);
                strikeSeekBar.setSelectedMaxValue(highStrikePriceIndexTemp);
                strikeSeekBar.setNotifyWhileDragging(true);
                strikeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    Toast toast;
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                        //Log.d("OpcionesMEFF","Valores obtenidos: Min = " + minValue + " Max = " + maxValue);
                        // strikeSeekBar.setSelectedMinValue(1.00f);
                        if(toast == null) {
                            toast = Toast.makeText(context, "STRIKE\n" + strikes.get(minValue) + " --- " + strikes.get(maxValue) + "\n", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL,0, -200);
                        }
                        lowStrikePriceIndexTemp = minValue;
                        highStrikePriceIndexTemp = maxValue;
                        toast.setText("STRIKE\n" + strikes.get(minValue) + " <-> " + strikes.get(maxValue));
                        toast.show();
                    }

                });

                dialog.show();
            }
        });

/*
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String ticker = ((StockID)spinner.getSelectedItem()).getTicker();
                    new AsyncURLCall(new AsyncURLCall.AsyncResponse() {
                        @Override
                        public void urlCallFinish(ArrayList<OptionRow> output) {
                            //TextView box = (TextView)findViewById(R.id.text);
                            //setTitle(ticker);
                            //box.setText(output);
                            //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
                        }
                    }).execute("http://www.meff.es/aspx/Financiero/Ficha.aspx?ticker=" + ticker);
                    Snackbar.make(view, "Called URL with ticker: "+ticker, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
*/

        SugarContext.init(context);

        //TEST DAO
        new TestDao().test();

        return view;

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onFragmentInteraction(String id) {
//        Toast.makeText(getApplicationContext(),"Interaction",Toast.LENGTH_SHORT);
//    }

    private void refreshTable(){
        String deadline = new String();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");

        optionTable.removeAllViews();

        if(showDailyVolume){
            optionTable.setColumnCollapsed(0,false);
            optionTable.setColumnCollapsed(1,false);
            optionTable.setColumnCollapsed(9,false);
            optionTable.setColumnCollapsed(10,false);
            optionTableHeader.setColumnCollapsed(0,false);
            optionTableHeader.setColumnCollapsed(1,false);
            optionTableHeader.setColumnCollapsed(9,false);
            optionTableHeader.setColumnCollapsed(10,false);
        } else {
            optionTable.setColumnCollapsed(0,true);
            optionTable.setColumnCollapsed(1,true);
            optionTable.setColumnCollapsed(9,true);
            optionTable.setColumnCollapsed(10,true);
            optionTableHeader.setColumnCollapsed(0,true);
            optionTableHeader.setColumnCollapsed(1,true);
            optionTableHeader.setColumnCollapsed(9,true);
            optionTableHeader.setColumnCollapsed(10,true);
        }

        if(showMidPoint_BidAsk == BIDASK){
            optionTable.setColumnCollapsed(2,false);
            optionTable.setColumnCollapsed(4,false);
            optionTable.setColumnCollapsed(3,true);
            optionTableHeader.setColumnCollapsed(2,false);
            optionTableHeader.setColumnCollapsed(4,false);
            optionTableHeader.setColumnCollapsed(3,true);
            optionTable.setColumnCollapsed(6,false);
            optionTable.setColumnCollapsed(8,false);
            optionTable.setColumnCollapsed(7,true);
            optionTableHeader.setColumnCollapsed(6,false);
            optionTableHeader.setColumnCollapsed(8,false);
            optionTableHeader.setColumnCollapsed(7,true);
        } else {
            optionTable.setColumnCollapsed(2,true);
            optionTable.setColumnCollapsed(4,true);
            optionTable.setColumnCollapsed(3,false);
            optionTableHeader.setColumnCollapsed(2,true);
            optionTableHeader.setColumnCollapsed(4,true);
            optionTableHeader.setColumnCollapsed(3,false);
            optionTable.setColumnCollapsed(6,true);
            optionTable.setColumnCollapsed(8,true);
            optionTable.setColumnCollapsed(7,false);
            optionTableHeader.setColumnCollapsed(6,true);
            optionTableHeader.setColumnCollapsed(8,true);
            optionTableHeader.setColumnCollapsed(7,false);
        }

        if(optionRows.size() == 0){
            Toast.makeText(context,R.string.no_data_from_MEFF,Toast.LENGTH_LONG);
        }

        for(OptionRow optionRow: optionRows){
//                    optionRow.save();

            if(Float.parseFloat(optionRow.getStrike()) < strikes.get(lowStrikePriceIndex) || Float.parseFloat(optionRow.getStrike()) > strikes.get(highStrikePriceIndex)){
                continue;
            }
            try {
                if(format.parse(optionRow.getDeadline()).before(expirationDates.get(nearExpirationDateIndex)) ||
                   format.parse(optionRow.getDeadline()).after(expirationDates.get(farExpirationDateIndex))){
                    continue;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(!(deadline.contentEquals(optionRow.getDeadline()))){
                TableRow rowExp = new TableRow(context);
                deadline = optionRow.getDeadline();
                TextView textExp = new TextView(context);
                textExp.setText(optionRow.getDeadline());
//                textExp.setBackgroundColor(Color.BLACK);
                textExp.setTextColor(Color.RED);
                rowExp.addView(textExp);
                TableRow.LayoutParams params = (TableRow.LayoutParams) textExp.getLayoutParams();
//                params.span = 11;
//                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.gravity = Gravity.CENTER_HORIZONTAL;
                params.column =5;
                textExp.setLayoutParams(params);

                optionTable.addView(rowExp);
            }

            TableRow row = new TableRow(context);
            TableRow.LayoutParams params2;
            TextView text;
            // Day volume call
            text = new TextView(context);
            text.setText(optionRow.getDayVolumeCall());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Last price call
            text = new TextView(context);
            text.setText(optionRow.getLastPriceCall());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Bid price call
            text = new TextView(context);
            text.setText(optionRow.getSellPriceCall());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Mid Point call
            text = new TextView(context);
            text.setText("Err");
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Ask price call
            text = new TextView(context);
            text.setText(optionRow.getBuyPriceCall());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Strike call
            text = new TextView(context);
            text.setText(optionRow.getStrike());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Bid price put
            text = new TextView(context);
            text.setText(optionRow.getSellPricePut());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Mid Point put
            text = new TextView(context);
            text.setText("Err");
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Ask price put
            text = new TextView(context);
            text.setText(optionRow.getBuyPricePut());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Last price call
            text = new TextView(context);
            text.setText(optionRow.getLastPricePut());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);
            // Day volume call
            text = new TextView(context);
            text.setText(optionRow.getDayVolumePut());
            row.addView(text);
            params2 = (TableRow.LayoutParams) text.getLayoutParams();
            params2.gravity = Gravity.CENTER_HORIZONTAL;
            params2.weight = 1;
            text.setLayoutParams(params2);

            optionTable.addView(row);
        }
    }

}
