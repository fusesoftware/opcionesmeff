package com.fusesoftware.fintechapps.opcionesmeff.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.ValuesES;
import com.fusesoftware.fintechapps.opcionesmeff.model.AsyncURLCall;
import com.fusesoftware.fintechapps.opcionesmeff.model.OptionRow;
import com.fusesoftware.fintechapps.opcionesmeff.receiver.AlarmReceiver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mllorente on 09/03/2016.
 *
 *
 *
 *
 * //TODO poner en marcha este servicio
 * //TODO actualizar cuando abre la app y mientras está en ella
 * //TODO actualizar SIEMPRE antes de cierre de mercado
 *
 */
public class UpdateService extends Service{

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Servicio opciones MEFF OnStartCommand", Toast.LENGTH_LONG).show();
        Log.d(this.getClass().getName(), "Servicio opciones MEFF OnStartCommand");
        LinkedList<String> tickerList = (LinkedList<String>) ValuesES.getTickerList();
        for (String ticker : tickerList) {

            new AsyncURLCall(new AsyncURLCall.AsyncResponse() {
                @Override
                public void urlCallFinish(ArrayList<OptionRow> output) {
                    for (OptionRow optionRow : output) {
                        //Log.d(this.getClass().getName(),"Saving a row: "+optionRow);
                        //optionRow.save();
                    }
                }
            }).execute("http://www.meff.es/aspx/Financiero/Ficha.aspx?ticker=" + ticker);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
