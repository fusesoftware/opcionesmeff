package com.fusesoftware.fintechapps.opcionesmeff;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.fusesoftware.fintechapps.opcionesmeff.data.common.CommonConfig;
import com.fusesoftware.fintechapps.opcionesmeff.model.MarketCloseManager;
import com.fusesoftware.fintechapps.opcionesmeff.model.TestDao;
import com.fusesoftware.fintechapps.opcionesmeff.receiver.AlarmReceiver;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_bar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SectionsPagerAdapter(getFragmentManager(),MainActivity.this));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        //TestAlarm.test(this);

        SharedPreferences preferences;
        preferences = this.getSharedPreferences(CommonConfig.SHARED_PREFERENCES,MODE_PRIVATE);
        if(!preferences.contains(CommonConfig.PREF_MARKET_CLOSE_ALARM)){
            new MarketCloseManager(this).scheduleLastUpdateAt(CommonConfig.lastUpdateHour,CommonConfig.lastUpdateMinutes);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 3;
        private Context context;
        Fragment optionData, strategy, portfolio;

        public SectionsPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    if(optionData == null){
                        fragment = new OptionDataFragment();
                        if(fragment instanceof OptionDataFragment){
                            ((OptionDataFragment)fragment).setContext(context);
                        }

                        optionData = fragment;
                    } else {
                        fragment = optionData;
                    }
                    break;
                case 1:
                    if(strategy == null){
                        fragment = new StrategyFragment();
                        strategy = fragment;
                    } else {
                        fragment = strategy;
                    }
                    break;
                case 2:
                    if(portfolio == null){
                        fragment = new PortfolioFragment();
                        portfolio = fragment;
                    } else {
                        fragment = portfolio;
                    }
                    break;
                default:
                    if(optionData == null){
                        fragment = new OptionDataFragment();
                        optionData = fragment;
                    } else {
                        fragment = optionData;
                    }
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.tab1);
                case 1:
                    return getString(R.string.tab2);
                case 2:
                    return getString(R.string.tab3);
            }
            return null;
        }
    }



}
