package com.fusesoftware.fintechapps.opcionesmeff.conectivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by julian on 24/01/16.
 */
public abstract class DownloadFileFromURL extends AsyncTask <String,String,String> {

    private Context context;
    private ProgressDialog pDialog;

    public DownloadFileFromURL(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(context != null){
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Descargando archivo");
            pDialog.setProgress(0);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        pDialog.setProgress(Integer.getInteger(values[0]));
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection; /*= url.openConnection();*/
            //7connection.connect();

        } catch (Exception e) {  }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPreExecute();

        if(context!=null){
            pDialog.dismiss();
        }
    }
}
