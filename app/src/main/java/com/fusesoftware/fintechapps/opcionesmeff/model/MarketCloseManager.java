package com.fusesoftware.fintechapps.opcionesmeff.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.CommonConfig;
import com.fusesoftware.fintechapps.opcionesmeff.receiver.AlarmReceiver;

import java.util.Calendar;

/**
 * Created by Miguel A. Llorente on 04/07/2016.
 */
public class MarketCloseManager {

    private Context context;
    private AlarmManager manager;


    public MarketCloseManager(Context context){
        this.context = context;
    }

    public AlarmManager scheduleLastUpdateAt(int lastUpdateHour, int lastUpdateMinutes) {
        PendingIntent pendingIntent;
        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        SharedPreferences preferences = context.getSharedPreferences(CommonConfig.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        preferences.edit().putInt(CommonConfig.PREF_MARKET_CLOSE_ALARM,pendingIntent.getCreatorUid()).commit();
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        /* Set the alarm to start at a given hour*/
        Calendar customTime = Calendar.getInstance();
        customTime.setTimeInMillis(System.currentTimeMillis());
        customTime.set(Calendar.HOUR_OF_DAY,lastUpdateHour);
        customTime.set(Calendar.MINUTE,lastUpdateMinutes);
        manager.set(AlarmManager.RTC_WAKEUP,customTime.getTimeInMillis(),pendingIntent);
        return manager;
    }

    public void cancel(){
        PendingIntent pendingIntent;
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }

}
