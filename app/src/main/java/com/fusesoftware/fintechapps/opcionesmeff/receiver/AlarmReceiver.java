package com.fusesoftware.fintechapps.opcionesmeff.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.fusesoftware.fintechapps.opcionesmeff.data.common.ValuesES;
import com.fusesoftware.fintechapps.opcionesmeff.model.AsyncURLCall;
import com.fusesoftware.fintechapps.opcionesmeff.model.OptionRow;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.BrokenBarrierException;

/**
 * Created by Miguel A. Llorente on 04/07/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    //public static final String UPDATE_BEFORE_MARKET_CLOSES = "com.fusesoftware.fintechapps.opcionesmeff.services.UPDATE_BEFORE_CLOSE";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(this.getClass().getName(), "Servicio opciones MEFF OnStartCommand");
        LinkedList<String> tickerList = (LinkedList<String>) ValuesES.getTickerList();
        for (String ticker : tickerList) {

            new AsyncURLCall(new AsyncURLCall.AsyncResponse() {
                @Override
                public void urlCallFinish(ArrayList<OptionRow> output) {
                    for (OptionRow optionRow : output) {
                        //Log.d(this.getClass().getName(),"Saving a row: "+optionRow);
                        //TODO guardar
                        optionRow.saveRow();
                    }
                }
            }).execute("http://www.meff.es/aspx/Financiero/Ficha.aspx?ticker=" + ticker);
        }
    }
}
