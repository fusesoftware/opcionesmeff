package com.fusesoftware.fintechapps.opcionesmeff.model;

import android.util.Log;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by mllorente on 15/02/2016.
 * <p/>
 * //TODO only for representation. Remove in the final version.
 */
public class OptionRow {

    private static final String TAG = "OPTION POJO";
    private final DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
    private final DateFormat deadlineFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final DateFormat updateTimeFormat = new SimpleDateFormat("HH:mm");
    private String buyPriceCall = "";
    private String buyVolumeCall = "";
    private String sellPriceCall = "";
    private String sellVolumeCall = "";
    private String dayVolumeCall = "";
    private String lastPriceCall = "";
    private String strike = "";
    private String deadline = "";
    private String updateTime = "";
    private String buyPricePut = "";
    private String buyVolumePut = "";
    private String sellPricePut = "";
    private String sellVolumePut = "";
    private String dayVolumePut = "";
    private String lastPricePut = "";

    private String ticker;


    public OptionRow() {

    }

    public OptionRow(String htmlRow, String ticker) {
        Log.d(TAG, "OptionRow constructor");
        String[] htmlRowArray = htmlRow.split(" ");
        ArrayList<String> htmlCleanArrayList = new ArrayList<>();
        if (htmlRowArray.length > 18) {
            for (String s : htmlRowArray) {
                if (s.length() > 0 && isAsciiPrintable(s)) {
                    htmlCleanArrayList.add(s);
                    //Log.d(TAG,s);
                }
            }
//        for(int i = 0 ; i<htmlRowArray.length;i++){
//            System.out.println("["+i+"]"+htmlRowArray[i]);h
//        }

            String firstColumn = htmlCleanArrayList.get(0);
            String lastColumn = htmlCleanArrayList.get(htmlCleanArrayList.size() - 1);
            updateTime = htmlCleanArrayList.get(0);
            prepareForParsing(htmlCleanArrayList);
            if (firstColumn.length() <= 1) {
                dayVolumeCall = "0";
                lastPriceCall = "0";
                buyVolumeCall = htmlCleanArrayList.get(2);
                buyPriceCall = htmlCleanArrayList.get(3);
                sellPriceCall = htmlCleanArrayList.get(4);
                sellVolumeCall = htmlCleanArrayList.get(5);
                deadline = htmlCleanArrayList.get(6) + "-" + toNumericMonth(htmlCleanArrayList.get(7)) + "-" + htmlCleanArrayList.get(8);
                strike = htmlCleanArrayList.get(10);
                buyVolumePut = htmlCleanArrayList.get(11);
                buyPricePut = htmlCleanArrayList.get(12);
                sellPricePut = htmlCleanArrayList.get(13);
                sellVolumePut = htmlCleanArrayList.get(14);
                if (lastColumn.length() <= 1) {
                    lastPricePut = "0";
                    dayVolumePut = "0";
                } else {
                    lastPricePut = htmlCleanArrayList.get(15);
                    dayVolumePut = htmlCleanArrayList.get(16);
                    if (updateTime.length() < 5 && htmlCleanArrayList.get(17).length() > 1) {
                        updateTime = htmlCleanArrayList.get(17);
                    }
                }
            } else {
                dayVolumeCall = htmlCleanArrayList.get(1);
                lastPriceCall = htmlCleanArrayList.get(2);
                buyVolumeCall = htmlCleanArrayList.get(3);
                buyPriceCall = htmlCleanArrayList.get(4);
                sellPriceCall = htmlCleanArrayList.get(5);
                sellVolumeCall = htmlCleanArrayList.get(6);
                deadline = htmlCleanArrayList.get(7) + "-" + toNumericMonth(htmlCleanArrayList.get(8)) + "-" + htmlCleanArrayList.get(9);
                strike = htmlCleanArrayList.get(11);
                buyVolumePut = htmlCleanArrayList.get(12);
                buyPricePut = htmlCleanArrayList.get(13);
                sellPricePut = htmlCleanArrayList.get(14);
                sellVolumePut = htmlCleanArrayList.get(15);
                if (lastColumn.length() <= 1) {
                    lastPricePut = "0";
                    dayVolumePut = "0";
                } else {
                    lastPricePut = htmlCleanArrayList.get(16);
                    dayVolumePut = htmlCleanArrayList.get(17);
                    if (updateTime.length() < 5 && htmlCleanArrayList.get(18).length() > 1) {
                        updateTime = htmlCleanArrayList.get(18);
                    }
                }
            }
            Log.d(TAG, this.toString());


        } else {
            //TODO raise an exception
            Log.e(TAG, "Error: malformed html row. Array has length " + htmlCleanArrayList.size());
        }

        this.ticker = ticker;
    }

    private String toNumericMonth(String s) {
        switch (s) {
            case "ene":
                return "01";
            case "feb":
                return "02";
            case "mar":
                return "03";
            case "abr":
                return "04";
            case "may":
                return "05";
            case "jun":
                return "06";
            case "jul":
                return "07";
            case "ago":
                return "08";
            case "sep":
                return "09";
            case "oct":
                return "10";
            case "nov":
                return "11";
            case "dic":
                return "12";
            default:
                return "00";
        }
    }

    public void saveRow() {
        if (!isEmpty()) {
            try {
                Option put = new Option(OptionType.PUT, ticker, Float.parseFloat(buyPricePut), Integer.parseInt(buyVolumePut), Float.parseFloat(sellPricePut), Integer.parseInt(sellVolumePut), Integer.parseInt(dayVolumePut), Float.parseFloat(lastPricePut), Float.parseFloat(strike), deadlineFormat.parse(deadline),updateTimeFormat.parse(updateTime));
                Option call = new Option(OptionType.CALL, ticker, Float.parseFloat(buyPriceCall), Integer.parseInt(buyVolumeCall), Float.parseFloat(sellPriceCall), Integer.parseInt(sellVolumeCall), Integer.parseInt(dayVolumeCall), Float.parseFloat(lastPriceCall), Float.parseFloat(strike), deadlineFormat.parse(deadline),updateTimeFormat.parse(updateTime));
                Log.d("DEBUGGING Option (put)", ""+put.toString());
                put.save();
                call.save();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public Option getPut(){
        try {
            return new Option(OptionType.PUT, ticker, Float.parseFloat(buyPricePut), Integer.parseInt(buyVolumePut), Float.parseFloat(sellPricePut), Integer.parseInt(sellVolumePut), Integer.parseInt(dayVolumePut), Float.parseFloat(lastPricePut), Float.parseFloat(strike), deadlineFormat.parse(deadline),updateTimeFormat.parse(updateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Option getCall(){
        try {
            return new Option(OptionType.CALL, ticker, Float.parseFloat(buyPriceCall), Integer.parseInt(buyVolumeCall), Float.parseFloat(sellPriceCall), Integer.parseInt(sellVolumeCall), Integer.parseInt(dayVolumeCall), Float.parseFloat(lastPriceCall), Float.parseFloat(strike), deadlineFormat.parse(deadline),updateTimeFormat.parse(updateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
        }

    public boolean isEmpty() {
        return buyPriceCall.isEmpty() &&
                buyVolumeCall.isEmpty() &&
                sellPriceCall.isEmpty() &&
                sellVolumeCall.isEmpty() &&
                dayVolumeCall.isEmpty() &&
                lastPriceCall.isEmpty() &&
                strike.isEmpty() &&
                deadline.isEmpty() &&
                updateTime.isEmpty() &&
                buyPricePut.isEmpty() &&
                buyVolumePut.isEmpty() &&
                sellPricePut.isEmpty() &&
                sellVolumePut.isEmpty() &&
                dayVolumePut.isEmpty() &&
                lastPricePut.isEmpty();
    }

    private void prepareForParsing(ArrayList<String> htmlCleanArrayList) {
        int i = 0;
        for (String s : htmlCleanArrayList
                ) {
            if (s.contains("-")) {
                htmlCleanArrayList.set(i, "0");
            }
            if (s.contains(",")) {
                htmlCleanArrayList.set(i, s.replace(",", "."));
            }
            i++;
        }
    }

    public static boolean isAsciiPrintable(String st) {
        return isAsciiPrintable(st.charAt(0));
    }

    public static boolean isAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127;
    }

    public String getBuyPriceCall() {
        return buyPriceCall;
    }

    public String getBuyVolumeCall() {
        return buyVolumeCall;
    }

    public String getSellPriceCall() {
        return sellPriceCall;
    }

    public String getSellVolumeCall() {
        return sellVolumeCall;
    }

    public String getStrike() {
        return strike;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getBuyPricePut() {
        return buyPricePut;
    }

    public String getBuyVolumePut() {
        return buyVolumePut;
    }

    public String getSellPricePut() {
        return sellPricePut;
    }

    public String getSellVolumePut() {
        return sellVolumePut;
    }

    public String getDayVolumeCall() {
        return dayVolumeCall;
    }

    public String getLastPriceCall() {
        return lastPriceCall;
    }

    public String getLastPricePut() {
        return lastPricePut;
    }

    public String getDayVolumePut() {
        return dayVolumePut;
    }


    @Override
    public String toString() {


        return "Date: " + updateTime +
                "\nDayVolCall " + dayVolumeCall +
                "\nLastPriceCall " + lastPriceCall +
                "\nBuyPriceCall " + buyPriceCall +
                "\nBuyVolCall " + buyVolumeCall +
                "\nSellPriceCall " + sellPriceCall +
                "\nSellVolCall " + sellVolumeCall +
                "\nStrike " + strike +
                "\nDeadline " + deadline +
                "\nDayVolPut " + dayVolumePut +
                "\nLastPricePut " + lastPricePut +
                "\nBuyPricePut " + buyPricePut +
                "\nBuyVolPut " + buyVolumePut +
                "\nSellPricePut " + sellPricePut +
                "\nSellVolPut " + sellVolumePut;

    }
}
